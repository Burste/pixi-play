module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module',
    allowImportExportEverywhere: true
  },
  plugins: ['react'],
  rules: {
    'comma-dangle': 'off',
    'generator-star-spacing': 'off',
    'no-underscore-dangle': 'off',
    'array-callback-return': 'off',
    'no-unused-vars': 'off',
    'space-before-function-paren': [
      'error',
      {
        anonymous: 'never',
        named: 'never',
        asyncArrow: 'never'
      }
    ],
    camelcase: 'off',
    'padded-blocks': 'off',
    'no-console': 'off',
    'object-property-newline': 'off',
    'no-use-before-define': ['error', { functions: true, classes: true }],
    'max-len': ['error', { code: 200, tabWidth: 2 }],
    'react/prop-types': 0,
    'react/react-in-jsx-scope': 'off'
    // standard: {
    //   'space-before-function-paren': 0
    // }
  }
}
