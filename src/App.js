import { assign, createMachine } from 'xstate'
import { useMachine } from '@xstate/react'
// import { lightMachine } from './lightMachine'

const LIGHT_STATES = {
  RED: 'RED',
  GREEN: 'GREEN',
  YELLOW: 'YELLOW'
}

const LIGHT_EVENTS = {
  CLICK: 'CLICK'
}

const actions = {
  msg: assign({
    count: (ctx, evt) => console.log('count', ctx, evt)
  }),
  entryRed: assign((context, event) =>
    console.log('entry red', context, event)
  ),
  exitRed: assign((context, event) => {
    console.log('exit red', context, event)
    return {
      count: context.count - event.value
    }
  })
}

export const lightMachine = createMachine(
  {
    initial: LIGHT_STATES.RED,
    context: {
      count: 0,
      user: null
    },
    states: {
      [LIGHT_STATES.RED]: {
        entry: ['entryRed'],
        exit: ['exitRed'],
        on: {
          [LIGHT_EVENTS.CLICK]: {
            target: LIGHT_STATES.GREEN
          }
        }
      },
      [LIGHT_STATES.GREEN]: {
        on: {
          [LIGHT_EVENTS.CLICK]: LIGHT_STATES.YELLOW
        }
      },
      [LIGHT_STATES.YELLOW]: {
        on: {
          [LIGHT_EVENTS.CLICK]: LIGHT_STATES.RED
        }
      }
    }
  },
  { actions }
)

const App = () => {
  const [state, send] = useMachine(lightMachine)
  return (
    <div className='App'>
      {state.matches(LIGHT_STATES.RED) && <p>RED</p>}
      {state.matches(LIGHT_STATES.GREEN) && <p>GREEN</p>}
      {state.matches(LIGHT_STATES.YELLOW) && <p>YELLOW</p>}
      <button
        onClick={() => {
          send({ type: LIGHT_EVENTS.CLICK, value: 123 })
        }}
      >
        CLICK
      </button>
    </div>
  )
}

export default App
