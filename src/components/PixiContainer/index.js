import { Stage } from '@inlet/react-pixi'
import { useState } from 'react'

const DEFAULT_CONFIG = {
  width: 400,
  height: 400,
  backgroundColor: 0x1d2330
}
export const PixiStage = props => {
  const [config] = useState({ ...DEFAULT_CONFIG, ...props })

  return <Stage {...config} />
}
